import { AuthService } from "./auth.service";

describe("AuthService", () => {
  describe("isAuthenticated", () => {
    let service: AuthService;

    beforeEach(() => {
      service = new AuthService();
      localStorage.setItem("token", "Bearer eysjafhkjaHFASHFKJaashfgj");
    });

    afterEach(() => {
      localStorage.removeItem("token");
    });

    it("should check if user is authenticated", (done) => {
      service.isAuthenticated().then((isAuth) => {
        expect(isAuth).toBe(true);
        done();
      });
    });
  });
});
