export class WelcomeComponent {
  greet(name: string): string {
    return `Welcome ${name}!`;
  }
}
