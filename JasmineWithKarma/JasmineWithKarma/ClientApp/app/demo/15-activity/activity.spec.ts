fdescribe("Activity #1", () => {
  it("should be equal", () => {
    expect([]).toEqual([]);
    expect([]).toBeTruthy();
  });
});

class Person {
  fullname: string;
  constructor() {
    setTimeout(() => {
      this.fullname = "Walter White";
    }, 1000);
  }
}

fdescribe("Activity #2", () => {
  it("should set fullname", () => {
    const person = new Person();

    setTimeout(() => {
      expect(person.fullname).toBe("Walter White");
    }, 1500);
  });
});
