import { UserComponent } from "./user.component";
import { UserService } from "../services/user.service";
import { User } from "../models/user.model";

describe("UserComponent", () => {
  const mockUsers: User[] = [
    new User(1, "Eduard", "eduard@email.com"),
    new User(2, "Dummy User", "dummy@email.com"),
  ];

  let mockUserService: UserService;
  let component: UserComponent;
  let getUsersSpy: jasmine.Spy;
  let initUsersSpy: jasmine.Spy;

  beforeEach(() => {
    mockUserService = new MockUserService() as UserService;
    component = new UserComponent(mockUserService);
  });

  describe("ngOnInit", () => {
    it("should call the initializeUsers function", () => {
      initUsersSpy = spyOn<any>(component, "initializeUsers");

      component.ngOninit();

      expect(initUsersSpy).toHaveBeenCalled();
    });
  });

  describe("initializeUsers", () => {
    beforeEach(() => {
      getUsersSpy = spyOn(mockUserService, "getUsers");
    });

    it("should call the getUsers function", () => {
      component["initializeUsers"]();

      expect(getUsersSpy).toHaveBeenCalled();
    });

    it("should set users if service returns list", () => {
      // getUsersSpy.and.returnValue(mockUsers);
      getUsersSpy.and.callFake((callback) => {
        callback(mockUsers);
      });

      component["initializeUsers"]();

      expect(component.users).toBe(mockUsers);
      expect(component.users.length).toBeGreaterThan(0);
    });

    it("should not set users if service returns null", () => {
      getUsersSpy.and.callFake((callback) => {
        callback(null);
      });

      component["initializeUsers"]();

      expect(component.users).toBeUndefined();
    });
  });
});

class MockUserService {
  getUsers(callback): void {}
}
