﻿export class VoteComponent {
  voteCount: number = 0;

  constructor() {}

  upVote(): void {
    this.voteCount++;
  }

  downVote(): void {
    this.voteCount--;
  }
}
